﻿namespace Zakupki.Domain.TradingFloorInfoAggregate;

public enum TradingFloorStatus
{
    Отключен,
    Работает,
    Тестируется
}