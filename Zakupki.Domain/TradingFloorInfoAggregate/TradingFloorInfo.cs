﻿namespace Zakupki.Domain.TradingFloorInfoAggregate;

public class TradingFloorInfo
{
    public long Id { get; set; }

    public string Name { get; set; }

    public string PathToImg { get; set; }

    public TradingFloorStatus Status { get; set; }

    public int Order { get; set; }

    public float SecondsBetweenRequest { get; set; } = 1.5f;
}