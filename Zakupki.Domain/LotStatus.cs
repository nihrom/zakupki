﻿namespace Zakupki.Domain;

public enum LotStatus
{
    Новый,
    Закрытый,
    Пропущенный
}