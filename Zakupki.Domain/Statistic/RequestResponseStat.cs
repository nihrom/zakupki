﻿namespace Zakupki.Domain.Statistic;

public class RequestResponseStat
{
    public int RequestsCount { get; set; }
    public int ResponsesCount { get; set; }
}