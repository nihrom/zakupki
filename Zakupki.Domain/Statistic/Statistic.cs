﻿namespace Zakupki.Domain.Statistic
{
    public class Statistic
    {
        public Dictionary<string, RequestResponseStat> RequestResponseStats = new Dictionary<string, RequestResponseStat>();
        public int SendOutBot = 0;

        public void AddPureRequest(string floor, string searchName)
        {
            string key = floor + "-" + searchName;

            if (RequestResponseStats.TryGetValue(key, out var stat))
            {
                RequestResponseStats[key].RequestsCount = stat.RequestsCount + 1;
            }
            else
            {
                RequestResponseStats.TryAdd(key, new RequestResponseStat() { RequestsCount = 1 });
            }
        }

        public void AddSuccResponse(string floor, string searchName)
        {
            string key = floor + "-" + searchName;

            if (RequestResponseStats.TryGetValue(key, out var stat))
            {
                RequestResponseStats[key].ResponsesCount = stat.ResponsesCount + 1;
            }
            else
            {
                RequestResponseStats.TryAdd(key, new RequestResponseStat() { ResponsesCount = 1 });
            }
        }
    }
}
