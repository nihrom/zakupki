﻿namespace Zakupki.Domain;

public class Lot
{
    public string Id { get; set; }

    public string InnerId { get; set; }
    
    public string Name { get; set; }

    public string? PublicationDate { get; set; }

    public DateTime SearchDate { get; set; }

    public DateTime? ClosingDate { get; set; }
    
    public string CompanyName { get; set; }
    
    public string Price { get; set; }
    
    public string Link { get; set; }

    public LotStatus Status { get; set; }

    public long TradingFloorInfoId { get; set; }
}