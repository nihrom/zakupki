#!/bin/bash

docker-compose build

for img in $(docker-compose config | awk '{if ($1 == "image:") print $2;}'); do
  images="$images $img"
done

echo $images


 docker image save $images | docker -H "ssh://zakupki2" image load
 docker-compose -H "ssh://root@zakupki2" up --force-recreate -d
 docker-compose -H "ssh://root@zakupki2" logs -f
 read -p "Press any key to continue... " -n1 -s