﻿using System;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.UI;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;

[assembly: HostingStartup(typeof(Zakupki.Areas.Identity.IdentityHostingStartup))]
namespace Zakupki.Areas.Identity
{
    public class IdentityHostingStartup : IHostingStartup
    {
        public void Configure(IWebHostBuilder builder)
        {
            builder.ConfigureServices((context, services) =>
            {
                //services.AddDbContext<ZakupkiDbContext>(opt => opt.UseSqlite("Data Source=Zakupkidb.db"), contextLifetime: ServiceLifetime.Transient,
                //optionsLifetime: ServiceLifetime.Singleton);
                //services.AddDbContextFactory<ZakupkiDbContext>(opt => opt.UseSqlite("Data Source=Zakupki.db"));

                //services.AddTransient<ZakupkiDbContext>(p =>
                //    p.GetRequiredService<IDbContextFactory<ZakupkiDbContext>>()
                //    .CreateDbContext());
            });
        }
    }
}