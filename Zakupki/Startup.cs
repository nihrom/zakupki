using System;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server;
using Microsoft.AspNetCore.DataProtection;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Polly;
using Radzen;
using Serilog;
using Serilog.Events;
using Zakupki.Application;
using Zakupki.Application.Hubs;
using Zakupki.Application.Parsers;
using Zakupki.DataAccess;
using Zakupki.Infrastructure;
using Zakupki.Infrastructure.Parsers;

namespace Zakupki;

public class Startup
{
    private readonly IConfiguration configuration;
    private readonly IWebHostEnvironment hostEnvironment;

    public Startup(IConfiguration configuration, IWebHostEnvironment hostEnvironment)
    {
        this.configuration = configuration;
        this.hostEnvironment = hostEnvironment;
    }

    public void ConfigureServices(IServiceCollection services)
    {
        services.AddApplication();
        services.AddDataAccess(configuration);
        services.AddInfrastructure(hostEnvironment.IsProduction());
        
        services.AddDataProtection()
            .PersistKeysToDbContext<ZakupkiDbContext>();

        services.AddRazorPages();
        services.AddServerSideBlazor();
        //services.AddSingleton<Statistic>();
        services.AddSingleton<ParsersSystem>();
        services.AddSingleton<PiplineFilter>();
        services.AddScoped<DialogService>();
        services.AddScoped<NotificationService>();
        services.AddScoped<TooltipService>();
        services.AddScoped<ContextMenuService>();
        services.AddTransient<ParserFactory>();

        services.AddScoped<IHostEnvironmentAuthenticationStateProvider>(sp =>
            (ServerAuthenticationStateProvider)sp.GetRequiredService<AuthenticationStateProvider>()
        );

        AddParserClients(services);
    }

    public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
    {
        //app.UseSerilogRequestLogging();

        if (env.IsDevelopment())
        {
            app.UseDeveloperExceptionPage();
        }
        else
        {
            app.UseExceptionHandler("/Error");
            // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
            app.UseHsts();
        }

        // app.ApplicationServices.GetRequiredService<DbSeed>();
        //DbSeed.EnsurePopulated(app);

        app.UseHttpsRedirection();
        app.UseStaticFiles();

        app.UseRouting();

        app.UseAuthentication();
        app.UseAuthorization();

        app.UseEndpoints(endpoints =>
        {
            endpoints.MapControllers();
            endpoints.MapBlazorHub();
            endpoints.MapFallbackToPage("/_Host");
            endpoints.MapHub<LotHub>("/lotsHub");
        });
    }

    private void AddParserClients(IServiceCollection services)
    {
        services.AddHttpClient("BEREZKA", client =>
            {
                client.BaseAddress = new Uri("https://tender-cache-api.agregatoreat.ru/");
                // client.DefaultRequestHeaders.Add("User-Agent",
                //     "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/123.0.0.0 Safari/537.36");
                client.DefaultRequestHeaders.Add("User-Agent", "Other");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("Sber-AST", client =>
            {
                client.BaseAddress = new Uri("https://www.sberbank-ast.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("ETP-GPB", client =>
            {
                client.BaseAddress = new Uri("https://etpgpb.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("RTS", client =>
            {
                client.BaseAddress = new Uri("https://www.rts-tender.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("ETPRF", client =>
            {
                client.BaseAddress = new Uri("http://etprf.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("ZakazRF", client =>
            {
                client.BaseAddress = new Uri("http://zakazrf.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("ETP-ETS", client =>
            {
                client.BaseAddress = new Uri("https://etp-ets.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("TEKTORG", client =>
            {
                client.BaseAddress = new Uri("https://www.tektorg.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("LOT-ONLINE", client =>
            {
                client.BaseAddress = new Uri("https://msp.lot-online.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("OTC", client =>
            {
                client.BaseAddress = new Uri("https://otc.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("Roseltorg", client =>
            {
                client.BaseAddress = new Uri("https://roseltorg.ru/");
                client.DefaultRequestHeaders.Add("User-Agent",
                    "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );

        services.AddHttpClient("OldBotSender", client =>
            {
                client.BaseAddress = new Uri("http://zak.simplepro.ru/");
                client.DefaultRequestHeaders.Add("Cookie", "zak_auth=98FA1-21189-C33A1-1943B-88BC1");
            })
            .AddTransientHttpErrorPolicy(p => p.RetryAsync(3))
            .AddTransientHttpErrorPolicy(
                p => p.CircuitBreakerAsync(5, TimeSpan.FromSeconds(30))
            );
    }
}