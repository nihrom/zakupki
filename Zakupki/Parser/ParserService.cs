﻿using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Hosting;
using Serilog;
using Zakupki.Application.Parsers;

namespace Zakupki.Parser
{
    public class ParserService : BackgroundService
    {
        private readonly ILogger logger = Log.ForContext<ParserService>();
        private readonly ParsersSystem parsersSystem;

        public ParserService(
            ParsersSystem parsersSystem)
        {
            this.parsersSystem = parsersSystem;
        }
        
        protected override async Task ExecuteAsync(CancellationToken ct)
        {
            logger.Information("Сервис регулирующий парсеры запускается");
            
            await parsersSystem.StartAsync();
        }
        
        public override async Task StopAsync(CancellationToken stoppingToken)
        {
            logger.Information("Сервис регулирующий парсеры запускается завершает свою работу");
        
            await parsersSystem.StopAsync();
        
            await base.StopAsync(stoppingToken);
        }
    }
}