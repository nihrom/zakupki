﻿using Zakupki.Domain;

namespace Zakupki.Application.Services;

public interface IParser
{
    Task<IReadOnlyCollection<Lot>> ParseLotsAsync(string searchText, CancellationToken ct);

    public Task<string> ParseRawDataAsync(string searchText, CancellationToken ct);

    public Task<IReadOnlyCollection<string>> ParseProcessedDataAsync(string searchText, CancellationToken ct);
    
}