﻿using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;
using Zakupki.Domain.TradingFloorInfoAggregate;

namespace Zakupki.Application.Services.Parsers;

public interface IParserFactory
{
    (IReadOnlyCollection<string> searchArr, IParser parser) CreateParser(
        TradingFloorInfo tradingFloorInfo,
        IMemoryCache? memoryCache);
}