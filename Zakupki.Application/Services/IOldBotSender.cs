﻿using Zakupki.Domain;

namespace Zakupki.Application.Services
{
    public interface IOldBotSender
    {
        Task<bool> SendLotAsync(Lot lot);
    }
}
