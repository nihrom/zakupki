﻿using MassTransit;
using Microsoft.Extensions.DependencyInjection;
using Zakupki.Application.Consumers;
using Zakupki.Application.Services;
using MediatR;

namespace Zakupki.Application;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddApplication(this IServiceCollection services)
    {
        services.AddMediator(cfg =>
        {
            //cfg.AddConsumer<LotConsumer>();
            cfg.AddConsumer<OldBotSenderConsumer>();
            //cfg.AddConsumer<StatisticConsumer>();
        });
        
        services.AddMediatR(cfg => {
            cfg.RegisterServicesFromAssembly(typeof(ServiceCollectionExtensions).Assembly);
        });
        return services;
    }
}