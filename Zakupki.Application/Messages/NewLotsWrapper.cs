﻿using MediatR;
using Zakupki.Domain;

namespace Zakupki.Application.Messages;

public record NewLotsWrapper(
    IReadOnlyCollection<Lot> Lot,
    string FloorName,
    string SearchText) : INotification;