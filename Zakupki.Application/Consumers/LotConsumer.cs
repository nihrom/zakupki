﻿using MassTransit;
using MediatR;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.SignalR.Client;
using Zakupki.Application.Hubs;
using Zakupki.Application.Messages;

namespace Zakupki.Application.Consumers;

public class LotConsumer : INotificationHandler<NewLotsWrapper>
{
    private readonly IHubContext<LotHub> hubContext;

    public LotConsumer(IHubContext<LotHub> hubContext)
    {
        this.hubContext = hubContext;
    }

    public async Task Handle(NewLotsWrapper notification, CancellationToken cancellationToken)
    {
        Console.WriteLine("LotConsumer - " + notification.Lot.Count + " " + notification.FloorName);

        await hubContext.Clients.All.SendAsync("Lots", notification.Lot, cancellationToken);
    }
}