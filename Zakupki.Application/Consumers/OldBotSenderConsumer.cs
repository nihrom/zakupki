﻿using MassTransit;
using Zakupki.Application.Messages;

namespace Zakupki.Application.Consumers;

public class OldBotSenderConsumer : IConsumer<NewLotsWrapper>
{
    public Task Consume(ConsumeContext<NewLotsWrapper> context)
    {
        Console.WriteLine("OldBotSenderConsumer");
        
        return Task.CompletedTask;
    }
}