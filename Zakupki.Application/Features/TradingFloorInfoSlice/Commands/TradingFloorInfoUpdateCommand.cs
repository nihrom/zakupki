﻿using MediatR;
using Zakupki.Application.Features.TradingFloorInfoSlice.Events;
using Zakupki.Application.Persistence;
using Zakupki.Domain.TradingFloorInfoAggregate;

namespace Zakupki.Application.Features.TradingFloorInfoSlice.Commands;

public record TradingFloorInfoUpdateCommand(TradingFloorInfo TradingFloorInfo) 
    : IRequest;

internal class TradingFloorInfoUpdateCommandHandler 
    : IRequestHandler<TradingFloorInfoUpdateCommand>
{
    private readonly IMediator mediator;
    private readonly ITradingFloorInfosRepository tradingFloorInfosRepository;

    public TradingFloorInfoUpdateCommandHandler(
        IMediator mediator,
        ITradingFloorInfosRepository tradingFloorInfosRepository)
    {
        this.mediator = mediator;
        this.tradingFloorInfosRepository = tradingFloorInfosRepository;
    }

    public async Task Handle(
        TradingFloorInfoUpdateCommand request,
        CancellationToken cancellationToken)
    {
        Console.WriteLine("TradingFloorInfoUpdateCommandHandler");

        await tradingFloorInfosRepository.UpdateAsync(
            request.TradingFloorInfo,
            CancellationToken.None);
        
        await mediator.Publish(
            new TradingFloorInfoUpdateEvent(),
            CancellationToken.None);
    }
}