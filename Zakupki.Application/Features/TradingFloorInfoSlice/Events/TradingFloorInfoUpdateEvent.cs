﻿using MediatR;

namespace Zakupki.Application.Features.TradingFloorInfoSlice.Events;

public record TradingFloorInfoUpdateEvent() : INotification;

internal class TradingFloorInfoUpdateEventHandler 
    : INotificationHandler<TradingFloorInfoUpdateEvent>
{
    public async Task Handle(
        TradingFloorInfoUpdateEvent notification,
        CancellationToken cancellationToken)
    {
        Console.WriteLine("TradingFloorInfoUpdateEventHandler");
    }
}