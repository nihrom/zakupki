﻿namespace Zakupki.Application.Features.TradingFloorInfoSlice.Queries;

public record AllTradingFloorInfosQuery();