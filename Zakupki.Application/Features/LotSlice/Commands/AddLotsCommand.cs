﻿using MediatR;
using Zakupki.Application.Messages;
using Zakupki.Application.Persistence;
using Zakupki.Domain;

namespace Zakupki.Application.Features.LotSlice.Commands;

public record AddLotsCommand(IReadOnlyCollection<Lot> Lots, string FloorName) : IRequest;

internal class AddLotsCommandHandler : IRequestHandler<AddLotsCommand>
{
    private readonly ILotsRepository lotsRepository;
    private readonly IMediator mediator;

    public AddLotsCommandHandler(ILotsRepository lotsRepository, IMediator mediator)
    {
        this.lotsRepository = lotsRepository;
        this.mediator = mediator;
    }

    public async Task Handle(AddLotsCommand request, CancellationToken ct)
    {
        var newLots = new List<Lot>();
        
        foreach (var lot in request.Lots)
        {
            try
            {
                //await lotsRepository.AddAsync(lot);
                //newLots.Add(lot);
            }
            catch (Exception e)
            {
                //Console.WriteLine(e);
            }
        }

        await mediator.Publish(new NewLotsWrapper(newLots, request.FloorName, ""), ct);
    }
}