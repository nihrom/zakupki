﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Security.Cryptography;

namespace Zakupki.Utilites
{
    public class MD5Util
    {
        private readonly MD5 _md5;

        public MD5Util()
        {
            _md5 = MD5.Create();
        }

        public static string Calcule(string input)
        {
            var md5 = MD5.Create();
            var hash = md5.ComputeHash(Encoding.UTF8.GetBytes(input));

            return Convert.ToBase64String(hash);
        }

        public string CalculeLocal(string input)
        {
            var hash = _md5.ComputeHash(Encoding.UTF8.GetBytes(input));

            return Convert.ToBase64String(hash);
        }
    }
}
