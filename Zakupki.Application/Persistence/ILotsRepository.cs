﻿using Zakupki.Domain;

namespace Zakupki.Application.Persistence;

public interface ILotsRepository
{
    Task<IReadOnlyCollection<Lot>> GetAsync(LotStatus status);
    
    Task AddAsync(Lot lot);
    
    Task ClosedAsync(string id);
}