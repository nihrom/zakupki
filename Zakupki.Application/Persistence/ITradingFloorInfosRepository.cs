﻿using Zakupki.Domain;
using Zakupki.Domain.TradingFloorInfoAggregate;

namespace Zakupki.Application.Persistence;

public interface ITradingFloorInfosRepository
{
    Task<List<TradingFloorInfo>> GetAllAsync(CancellationToken ct = default);

    Task UpdateAsync(
        TradingFloorInfo tradingFloorInfo,
        CancellationToken ct = default);
}