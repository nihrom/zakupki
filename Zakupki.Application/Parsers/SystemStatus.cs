﻿namespace Zakupki.Application
{
    public enum SystemStatus
    {
        НеРаботает,
        Работает,
        Перезагружается
    }
}
