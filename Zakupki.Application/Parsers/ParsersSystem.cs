﻿using MediatR;
using Microsoft.Extensions.Caching.Memory;
using Serilog;
using Zakupki.Application.Persistence;
using Zakupki.Application.Services;
using Zakupki.Application.Services.Parsers;
using Zakupki.Domain;
using Zakupki.Domain.TradingFloorInfoAggregate;

namespace Zakupki.Application.Parsers;

public class ParsersSystem
{
    private readonly ILogger logger = Log.ForContext<ParsersSystem>();
    private readonly IParserFactory parserFactory;
    private readonly ITradingFloorInfosRepository tradingFloorInfosRepository;
    private Dictionary<string, Coordinator> coordinators;
    private readonly IMemoryCache memoryCache;
    private readonly IMediator mediator;

    private CancellationTokenSource? cancellationTokenSource;

    public ParsersSystem(
        IParserFactory parserFactory,
        ITradingFloorInfosRepository tradingFloorInfosRepository,
        IMemoryCache memoryCache, IMediator mediator)
    {
        this.parserFactory = parserFactory;
        this.tradingFloorInfosRepository = tradingFloorInfosRepository;
        this.memoryCache = memoryCache;
        this.mediator = mediator;
    }

    public SystemStatus Status { get; set; } = SystemStatus.НеРаботает;

    public async Task StartAsync()
    {
        logger.Information("ParsersSystem старт");
        coordinators = new();
        Status = SystemStatus.Работает;

        cancellationTokenSource = new CancellationTokenSource();
        var ct = cancellationTokenSource.Token;

        var tradingFloorInfos = (await tradingFloorInfosRepository.GetAllAsync(ct))
            .Where(t => t.Status == TradingFloorStatus.Работает)
            .ToList();

        foreach (var floor in tradingFloorInfos)
        {
            var coordinator = new Coordinator(parserFactory, floor, memoryCache, mediator);
            coordinators.Add(floor.Name, coordinator);

            coordinator.Start();
        }
    }

    public async Task RestartAsync()
    {
        logger.Information("ParsersSystem рестарт");

        Status = SystemStatus.Перезагружается;

        //await StopAsync();
        //await StartAsync();
    }

    public async Task StopAsync()
    {
        logger.Information("ParsersSystem стоп");

        foreach (var coordinatorsValue in coordinators.Values)
        {
            await coordinatorsValue.StopAsync();
        }
        
        cancellationTokenSource?.Cancel();
        cancellationTokenSource?.Dispose();
        cancellationTokenSource = null;

        Status = SystemStatus.НеРаботает;
    }
}