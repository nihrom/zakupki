﻿using MediatR;
using Microsoft.Extensions.Caching.Memory;
using Serilog;
using Zakupki.Application.Features.LotSlice.Commands;
using Zakupki.Application.Messages;
using Zakupki.Application.Services;
using Zakupki.Application.Services.Parsers;
using Zakupki.Domain;
using Zakupki.Domain.TradingFloorInfoAggregate;

namespace Zakupki.Application.Parsers;

public class Coordinator
{
    private readonly ILogger logger = Log.ForContext<Coordinator>();
    private readonly SearchQueries searchQueries;
    private readonly TradingFloorInfo tradingFloorInfo;
    private readonly IParser parser;
    private readonly IMediator mediator;
    private CancellationTokenSource cts = new (); //TODO: Посмотреть как правильно диспозить
    private Task? workTask;

    public Coordinator(
        IParserFactory parserFactory,
        TradingFloorInfo tradingFloorInfo,
        IMemoryCache memoryCache,
        IMediator mediator)
    {
        this.tradingFloorInfo = tradingFloorInfo;
        this.mediator = mediator;

        logger.Information(
            "Координатор для парсеров площадки {Name} создан",
            tradingFloorInfo.Name);

        var parsers = parserFactory.CreateParser(tradingFloorInfo, memoryCache);

        searchQueries = new SearchQueries(parsers.searchArr.ToList());

        parser = parsers.parser;
    }

    public void Start()
    {
        logger.Information(
            "Координатор для парсеров площадки {Name} запущен",
            tradingFloorInfo.Name);
        cts = new CancellationTokenSource();
        workTask = DoWorkAsync();
    }

    private async Task DoWorkAsync()
    {
        while (!cts.IsCancellationRequested)
        {
            await CoordinatorParseAsync(cts.Token);
            
            //TODO: Добавить в статистику получение запроса

            await Task.Delay(TimeSpan.FromSeconds(tradingFloorInfo.SecondsBetweenRequest), cts.Token);
        }
    }

    private async Task CoordinatorParseAsync(CancellationToken ct)
    {
        var searchText = searchQueries.NextQueryText();

        //TODO: Добавить в статистику отправку запроса
        try
        {
            var lots = await parser.ParseLotsAsync(searchText, ct);
            var lotResult = lots
                .Select(lot =>
                {
                    lot.TradingFloorInfoId = tradingFloorInfo.Id;

                    return lot;
                }).ToList();
            
            await mediator.Send(new AddLotsCommand(lotResult, tradingFloorInfo.Name), ct);
        }
        catch (Exception e)
        {
            //Console.WriteLine(e);
        }

        //TODO: Куда нибудь передавать полученные лоты
    }

    public async Task StopAsync()
    {
        logger.Information(
            "Координатор для парсеров площадки {Name} завершает свою работу",
            tradingFloorInfo.Name);
        
        cts.Cancel();
        if (workTask is not null)
            await workTask;

        workTask = null;
        cts.Dispose();
    }
}