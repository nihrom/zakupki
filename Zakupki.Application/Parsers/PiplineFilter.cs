﻿using Serilog;
using Zakupki.Domain;

namespace Zakupki.Application
{
    public class PiplineFilter
    {
        // private readonly ILogger logger = Log.ForContext<PiplineFilter>();
        // private readonly Dictionary<string, short> uniqueLots = new Dictionary<string, short>();
        // private readonly IDbContextFactory<ZakupkiDbContext> dbFactory;
        // private readonly LotPublisher lotPublisher;
        //
        // public PiplineFilter(IDbContextFactory<ZakupkiDbContext> dbFactory, LotPublisher lotPublisher)
        // {
        //     this.dbFactory = dbFactory;
        //     this.lotPublisher = lotPublisher;
        //
        //     using (var context = dbFactory.CreateDbContext())
        //     {
        //         var lots = context.Lots
        //             .AsNoTracking()
        //             .ToList();
        //
        //         lots.ForEach(lot => uniqueLots.Add(lot.Link, 0));
        //     }
        // }
        //
        // private async Task PushLot(WrapperLot wrapperLot)
        // {
        //     var isUniq = await UniqFilter(wrapperLot);
        //
        //     if (!isUniq)
        //         return;
        //
        //     lotPublisher.AddNewLot(wrapperLot);
        // }
        //
        // private async Task<bool> UniqFilter(WrapperLot lot)
        // {
        //     if (uniqueLots.ContainsKey(lot.Lot.Link))
        //         return false;
        //
        //     using (var context = dbFactory.CreateDbContext())
        //     {
        //         lot.Lot.SearchDate = DateTime.UtcNow;
        //         var lots = context.Lots.Add(lot.Lot);
        //         await context.SaveChangesAsync();
        //     }
        //
        //     uniqueLots.Add(lot.Lot.Link, 0);
        //
        //     logger.Information("Pipeline получил новый уникальный лот от площадки {0} - {1}", lot.FloorName, lot.Lot.Link);
        //
        //     return true;
        // }
    }
}
