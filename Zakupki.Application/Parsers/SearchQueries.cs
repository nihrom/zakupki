﻿namespace Zakupki.Application.Parsers;

public class SearchQueries
{
    private int iterator;

    private readonly List<string> searchTexts;

    public SearchQueries(List<string> searchTexts)
    {
        iterator = 0;
        this.searchTexts = searchTexts;
    }

    public string NextQueryText()
    {
        iterator++;
        if (iterator >= searchTexts.Count)
            iterator = 0;

        return searchTexts[iterator];
    }
}