﻿using Microsoft.AspNetCore.SignalR;
using Zakupki.Domain;

namespace Zakupki.Application.Hubs;

public class LotHub : Hub
{
    public const string HubUrl = "/lotsHub";
    
    public async Task Lots(IReadOnlyCollection<Lot> lots)
    {
        await Clients.All.SendAsync("Lots", lots);
    }
    
    public override Task OnConnectedAsync()
    {
        //await Groups.AddToGroupAsync(Context.ConnectionId, "SignalR Users");
        Console.WriteLine("OnConnectedAsync");
        return base.OnConnectedAsync();
    }
    
    public override async Task OnDisconnectedAsync(Exception e)
    {
        Console.WriteLine($"Disconnected {e?.Message} {Context.ConnectionId}");
        await base.OnDisconnectedAsync(e);
    }
}