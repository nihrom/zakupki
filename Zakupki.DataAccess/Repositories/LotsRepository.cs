﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Zakupki.Application.Persistence;
using Zakupki.Domain;

namespace Zakupki.DataAccess.Repositories;

public class LotsRepository : ILotsRepository
{
    private readonly IDbContextFactory<ZakupkiDbContext> dbFactory;
    private readonly IMapper mapper;

    public LotsRepository(IDbContextFactory<ZakupkiDbContext> dbFactory, IMapper mapper)
    {
        this.dbFactory = dbFactory;
        this.mapper = mapper;
    }

    public async Task<IReadOnlyCollection<Lot>> GetAsync(LotStatus status)
    {
        await using var context = await dbFactory.CreateDbContextAsync();
        
        var lots = context.Lots
            .Where(l => l.Status == status)
            .Join(context.TradingFloorInfos, l => l.TradingFloorInfoId, t => t.Id,
                (l, t) => l.SetTradingFloorInfo(t))
            .Take(20)
            .ToList();

        return mapper.Map<IReadOnlyCollection<Lot>>(lots);
    }

    public async Task ClosedAsync(string id)
    {
        await using var context = await dbFactory.CreateDbContextAsync();

        await context.Lots
            .Where(l => l.Id.Equals(id))
            .ExecuteUpdateAsync(
                s => s
                    .SetProperty(l => l.Status, LotStatus.Закрытый));

    }

    public async Task AddAsync(Lot lot)
    {
        var model = mapper.Map<Models.Lot>(lot);
        
        await using var context = await dbFactory.CreateDbContextAsync();

        await context.Lots
            .AddAsync(model);

        await context.SaveChangesAsync();
    }
}