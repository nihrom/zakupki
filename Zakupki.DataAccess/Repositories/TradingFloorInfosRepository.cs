﻿using AutoMapper;
using Microsoft.EntityFrameworkCore;
using Zakupki.Application.Persistence;
using Zakupki.Domain;
using Zakupki.Domain.TradingFloorInfoAggregate;

namespace Zakupki.DataAccess.Repositories;

public class TradingFloorInfosRepository : ITradingFloorInfosRepository
{
    private readonly IDbContextFactory<ZakupkiDbContext> dbFactory;
    private readonly IMapper mapper;

    public TradingFloorInfosRepository(
        IDbContextFactory<ZakupkiDbContext> dbFactory,
        IMapper mapper)
    {
        this.dbFactory = dbFactory;
        this.mapper = mapper;
    }

    public async Task<List<TradingFloorInfo>> GetAllAsync(CancellationToken ct = default)
    {
        await using var context = await dbFactory.CreateDbContextAsync(ct);

        var result = await context.TradingFloorInfos
            .ToListAsync(ct);
        
        return mapper.Map<List<TradingFloorInfo>>(result);
    }
    
    public async Task UpdateAsync(
        TradingFloorInfo tradingFloorInfo,
        CancellationToken ct = default)
    {
        await using var context = await dbFactory.CreateDbContextAsync(ct);
        
        var model = mapper.Map<Models.TradingFloorInfo>(tradingFloorInfo);

        context.TradingFloorInfos.Update(model); //TODO: Почему не async?

        await context.SaveChangesAsync();
    }
}