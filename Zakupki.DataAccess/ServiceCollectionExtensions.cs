using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Zakupki.Application.Persistence;
using Zakupki.Application.Services;
using Zakupki.DataAccess.Models;
using Zakupki.DataAccess.Repositories;

namespace Zakupki.DataAccess;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddDataAccess(this IServiceCollection services, IConfiguration configuration)
    {
        services.AddIdentity<IdentityUser, IdentityRole>()
            .AddEntityFrameworkStores<ZakupkiDbContext>();

        services.AddDbContextFactory<ZakupkiDbContext>(options =>
            options.UseNpgsql(configuration["DB:ConnectionString"]));

        services.AddDbContext<ZakupkiDbContext>(options =>
            options.UseNpgsql(configuration["DB:ConnectionString"]),
            ServiceLifetime.Transient,
            ServiceLifetime.Singleton);

        services.Configure<IdentityOptions>(options =>
        {
            // Password settings.
            options.Password.RequireDigit = false;
            options.Password.RequireLowercase = false;
            options.Password.RequireNonAlphanumeric = false;
            options.Password.RequireUppercase = false;
            options.Password.RequiredLength = 6;
            options.Password.RequiredUniqueChars = 1;

            // Lockout settings.
            options.Lockout.DefaultLockoutTimeSpan = TimeSpan.FromMinutes(5);
            options.Lockout.MaxFailedAccessAttempts = 5;
            options.Lockout.AllowedForNewUsers = true;

            // User settings.
            options.User.AllowedUserNameCharacters =
                "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-._@+";
            options.User.RequireUniqueEmail = false;
        });
        
        services.AddAutoMapper(typeof(MapperProfile));

        services.AddSingleton<ILotsRepository, LotsRepository>();
        services.AddSingleton<ITradingFloorInfosRepository, TradingFloorInfosRepository>();

        return services;
    }
}