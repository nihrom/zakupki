﻿using System.Diagnostics.CodeAnalysis;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Zakupki.DataAccess.Models;

namespace Zakupki.DataAccess;

[SuppressMessage("ReSharper", "ConvertToConstant.Local")]
[SuppressMessage("ReSharper", "InvertIf")]
public class DbSeed
{
    public static async Task EnsurePopulated(
        ZakupkiDbContext context,
        UserManager<IdentityUser> userManager,
        CancellationToken ct)
    {
        await context.Database.MigrateAsync(ct);

        if (!context.TradingFloorInfos.Any())
        {
            var tradingFloorInfos = new List<TradingFloorInfo>
            {
                new() { Name = "BEREZKA", PathToImg = "imgs/berezka.jpg", Order = 1 },
                new() { Name = "ETP-GPB", PathToImg = "imgs/etpgpb.jpg", Order = 2 },
                new() { Name = "Sber-AST", PathToImg = "imgs/sber-ast.jpg", Order = 3 },
                new() { Name = "RTS", PathToImg = "imgs/", Order = 4 },
                new() { Name = "ETPRF", PathToImg = "imgs/etprf.jpg", Order = 5 },
                new() { Name = "ZakazRF", PathToImg = "imgs/zakazrf.jpg", Order = 6 },
                new() { Name = "ETP-ETS", PathToImg = "imgs/etpets.jpg", Order = 7 },
                new() { Name = "TEKTORG", PathToImg = "imgs/tekatorg.jpg", Order = 8 },
                new() { Name = "LOT-ONLINE", PathToImg = "imgs/", Order = 9 },
                new() { Name = "OTC", PathToImg = "imgs/otc.jpg", Order = 10 },
                new() { Name = "Roseltorg", PathToImg = "imgs/roseltorg.jpg", Order = 11 }
            };

            context.TradingFloorInfos.AddRange(tradingFloorInfos);
            await context.SaveChangesAsync(ct);
        }

        if (!context.Users.Any())
        {
            var password = "13ED8-F736A-38FA1-78361";

            var appUser = new IdentityUser
            {
                UserName = "Admin"
            };

            await userManager.CreateAsync(appUser, password);
        }
    }
}