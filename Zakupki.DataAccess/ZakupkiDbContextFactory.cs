﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Design;

namespace Zakupki.DataAccess;

public class ZakupkiDbContextFactory: IDesignTimeDbContextFactory<ZakupkiDbContext>
{
    public ZakupkiDbContext CreateDbContext(string[] args)
    {
        var optionsBuilder = new DbContextOptionsBuilder<ZakupkiDbContext>();
        optionsBuilder.UseNpgsql("Host=localhost;Database=Zakupki;Username=postgres;Password=postgres");

        return new ZakupkiDbContext(optionsBuilder.Options);
    }
}