﻿using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Zakupki.DataAccess.Models;

namespace Zakupki.DataAccess;

public sealed class ZakupkiDbContext : IdentityDbContext<IdentityUser>, IDataProtectionKeyContext
{
    public ZakupkiDbContext(DbContextOptions<ZakupkiDbContext> options) : base(options)
    {
        
    }

    public DbSet<Lot> Lots { get; set; }

    public DbSet<TradingFloorInfo> TradingFloorInfos { get; set; }

    public DbSet<DataProtectionKey> DataProtectionKeys { get; set; } = null!;

    protected override void OnModelCreating(ModelBuilder builder)
    {
        builder.Entity<Lot>().HasKey(l => new { l.Id, l.TradingFloorInfoId });

        base.OnModelCreating(builder);
        // Customize the ASP.NET Identity model and override the defaults if needed.
        // For example, you can rename the ASP.NET Identity table names and more.
        // Add your customizations after calling base.OnModelCreating(builder);
    }
}