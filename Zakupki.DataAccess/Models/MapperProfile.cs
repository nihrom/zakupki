﻿using AutoMapper;

namespace Zakupki.DataAccess.Models;

public class MapperProfile : Profile
{
    public MapperProfile()
    {
        CreateMap<Domain.Lot, Lot>().ReverseMap();
        CreateMap<Domain.TradingFloorInfoAggregate.TradingFloorInfo, TradingFloorInfo>().ReverseMap();
    }
}