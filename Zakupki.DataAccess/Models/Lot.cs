﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Zakupki.Domain;

namespace Zakupki.DataAccess.Models;

public class Lot
{
    [StringLength(50)]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public string Id { get; set; }

    public string InnerId { get; set; }
    
    public string Name { get; set; }
    
    public string? PublicationDate { get; set; }
    
    public DateTime SearchDate { get; set; }

    public DateTime? ClosingDate { get; set; }
    
    public string CompanyName { get; set; }
    
    public string Price { get; set; }
    
    public string Link { get; set; }
    
    public LotStatus Status { get; set; }

    [DatabaseGenerated(DatabaseGeneratedOption.None)]
    public long TradingFloorInfoId { get; set; }

    [NotMapped] 
    public TradingFloorInfo TradingFloorInfo { get; set; }

    public Lot SetTradingFloorInfo(TradingFloorInfo tradingFloorInfo)
    {
        TradingFloorInfo = tradingFloorInfo;
        return this;
    }
}