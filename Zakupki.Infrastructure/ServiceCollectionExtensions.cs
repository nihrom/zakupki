﻿using Microsoft.Extensions.DependencyInjection;
using Zakupki.Application.Services;
using Zakupki.Application.Services.Parsers;
using Zakupki.Infrastructure.MailService;
using Zakupki.Infrastructure.OldBot;
using Zakupki.Infrastructure.Parsers;

namespace Zakupki.Infrastructure;

public static class ServiceCollectionExtensions
{
    public static IServiceCollection AddInfrastructure(this IServiceCollection services, bool isProduction)
    {
        services.AddTransient<IEmailService, EmailService>();

        if (isProduction)
            services.AddTransient<IOldBotSender, OldBotSender>();
        else
            services.AddTransient<IOldBotSender, FakeOldBotSender>();

        services.AddTransient<IParserFactory, ParserFactory>();
        
        return services;
    }
}