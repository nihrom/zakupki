﻿using Microsoft.Extensions.Caching.Memory;
using Serilog;
using Zakupki.Application.Services;
using Zakupki.Application.Services.Parsers;
using Zakupki.Domain;
using Zakupki.Domain.TradingFloorInfoAggregate;
using Zakupki.Infrastructure.Parsers.TradingFloorParsers;

namespace Zakupki.Infrastructure.Parsers
{
    public class ParserFactory : IParserFactory
    {
        private readonly IHttpClientFactory httpClientFactory;
        private readonly ILogger logger = Log.ForContext<ParserFactory>();

        public ParserFactory(IHttpClientFactory httpClientFactory)
        {
            this.httpClientFactory = httpClientFactory;
        }

        public (IReadOnlyCollection<string> searchArr, IParser parser) CreateParser(
            TradingFloorInfo tradingFloorInfo,
            IMemoryCache? memoryCache)
        {
            if (tradingFloorInfo.Name == "BEREZKA")
            {
                var searchArr = new[] { "1C", "1С", "неискл", "программн", "ЭВМ", "Битрикс", "лиценз" };

                return (searchArr, new Berezka(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "Sber-AST")
            {
                return (new []{ "1C" }, new SberAst(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "ETP-GPB")
            {
                var searchArr = new[] { "1C", "1С", "неискл", "программн", "ЭВМ", "Битрикс", "лиценз" };
                
                return (searchArr, new EtpGpb(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "RTS")
            {
                var searchArr = new[] { "1C", "1С", "неискл", "программн", "ЭВМ", "Битрикс", "лиценз" };
            
                return (searchArr, new Rts(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "ETPRF")
            {
                var searchArr = new[] { "1C", "1С", "неискл", "программн", "ЭВМ", "Битрикс", "лиценз" };
            
                return (searchArr, new EtpRf(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "ZakazRF")
            {
                var searchArr = new[] { "1C 1С неискл программн ЭВМ Битрикс лиценз" };
            
                return (searchArr, new Zakazrf(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "ETP-ETS")
            {
                var searchArr = new[] { "1C", "1С", "неискл", "программн", "ЭВМ", "Битрикс", "лиценз" };
            
                return (searchArr, new EtpEts(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "TEKTORG")
            {
                var searchArr = new[] { "1C", "1С", "неискл", "программн", "ЭВМ", "Битрикс", "лиценз" };
            
                return (searchArr, new TekTorg(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "LOT-ONLINE")
            {
                var searchArr = new[] { "1C", "1С", "неискл", "программн", "ЭВМ", "Битрикс", "лиценз" };
            
                return (searchArr, new LotOnline(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "OTC")
            {
                var searchArr = new[] { "[\"1C\", \"1С\"]", "[\"неискл\", \"ЭВМ\", \"Битрикс\", \"лиценз\"]" };
            
                return (searchArr, new Otc(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            if (tradingFloorInfo.Name == "Roseltorg")
            {
                var searchArr = new[] { "1C", "1С", "неискл", "программн", "ЭВМ", "Битрикс", "лиценз" };
            
                return (searchArr, new Roseltorg(httpClientFactory.CreateClient(tradingFloorInfo.Name), memoryCache));
            }

            logger.Error("Не удалось создать парсер для торговой площадки");

            throw new Exception();
        }
    }
}
