﻿using System.Web;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class EtpRf : Parser<string, IElement>
{
    public EtpRf(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
    }

    public override async Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<IElement> rawLots,
        CancellationToken ct)
    {
        var trs = rawLots;

        var lots = trs
            .Where(tr => tr.HasAttribute("id"))
            .Where(tr => !ContainedInTheCache(tr.TextContent))
            .Select(tr => new Lot
            {
                Link = "http://etprf.ru/" + tr.QuerySelector("a").GetAttribute("href"),
                Id = tr.ChildNodes[2].TextContent,
                Name = tr.ChildNodes[3].TextContent,
                Price = tr.ChildNodes[4].TextContent,
                CompanyName = tr.ChildNodes[7].TextContent,
                PublicationDate = tr.ChildNodes[8].TextContent,
                TradingFloorInfoId = -1
            })
            .ToList();

        return lots;
    }

    public override async Task<IReadOnlyCollection<IElement>> GetProcessedDataAsync(string rawData, CancellationToken ct)
    {
        var document = new HtmlParser().ParseDocument(rawData);
        var cellSelector = ".reporttable";
        var cell = document.QuerySelectorAll(cellSelector).FirstOrDefault();
        var trs = cell.QuerySelectorAll("tr");

        var lots = trs
            .Where(tr => tr.HasAttribute("id"))
            .Select(tr => tr)
            .ToList();

        return lots;
    }

    public override async Task<string> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var urlEncode = HttpUtility.UrlEncode(searchText);
        var url = "/NotificationCR?keyword=" + urlEncode + "&PublicationDateTimeFrom=" +
                  DateTime.Now.AddDays(-120).ToString("dd.MM.yyyy") + "&ExpandFilter=1";
        var response = await Client.GetAsync(url, ct);

        if (response.IsSuccessStatusCode)
        {
            var responseText = await response.Content.ReadAsStringAsync(ct);

            return responseText;
        }

        return string.Empty;
    }
}