﻿using System.Web;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class LotOnline : Parser<IHtmlElement, IElement>
{
    public LotOnline(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
    }

    public override Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<IElement> rawLots,
        CancellationToken ct)
    {
        throw new NotImplementedException();
    }

    public override async Task<IReadOnlyCollection<IElement>> GetProcessedDataAsync(IHtmlElement rawData, CancellationToken ct)
    {
        var trs = rawData.GetElementsByClassName("tender-item");

        var lots = trs
            .Select(tr => tr)
            .ToList();

        return lots;
    }

    public override async Task<IHtmlElement> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var document = await GetDateFromTradingPlaceAsync();

        return document.Body;
    }

    private async Task<IDocument> GetDateFromTradingPlaceAsync()
    {
        var urlencode = HttpUtility.UrlEncode("1С");
        var url = "https://msp.lot-online.ru/tenders/?SearchForm.Keywords=" + urlencode +
                  "&SearchForm.HasPrepayment=on&SearchForm.State=1&SearchForm.DocumentSearchEnabled=true&SearchForm.WithdrawnSearchEnabled=False&SearchForm.HasApplications=on&SearchForm.CurrencyCode=643&SearchForm.OrganizationLevels=Fz223&FilterData.SortingField=DatePublished&FilterData.SortingDirection=Desc&FilterData.PageSize=20&FilterData.PageIndex=1";
        Console.WriteLine(url);
        var config = Configuration.Default.WithDefaultLoader();
        var context = BrowsingContext.New(config);
        var document = await context.OpenAsync(url);

        return document;
    }
}