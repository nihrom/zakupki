﻿using System.Web;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class Rts : Parser<IHtmlElement, IElement>
{
    public Rts(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
    }

    public override async Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<IElement> rawLots,
        CancellationToken ct)
    {
        throw new NotImplementedException();
    }

    public override async Task<IReadOnlyCollection<IElement>> GetProcessedDataAsync(IHtmlElement rawData, CancellationToken ct)
    {
        throw new NotImplementedException();
        var document = await GetDateFromTradingPlaceAsync();
        var trs = document.Body.QuerySelectorAll("tr");

        var lots = trs
            .Skip(1)
            .Select(tr => tr)
            .ToList();

        return lots;
    }

    public override async Task<IHtmlElement> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        throw new NotImplementedException();
        var document = await GetDateFromTradingPlaceAsync();

        return document.Body;
    }

    private async Task<IDocument> GetDateFromTradingPlaceAsync()
    {
        throw new NotImplementedException();
        var urlencode = HttpUtility.UrlEncode("1С");
        var url = "https://etp-ets.ru/procedure/catalog/?&keywords=" + urlencode + "&publicationDateTime-from=" +
                  DateTime.Now.AddDays(-2).ToString("dd.MM.yyyy") + "&fullSearch=1";
        Console.WriteLine(url);
        var uri = new Uri(url);

        var config = Configuration.Default.WithDefaultLoader();
        var context = BrowsingContext.New(config);
        var document = await context.OpenAsync(url);

        return document;
    }
}