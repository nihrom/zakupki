﻿using System.Text.RegularExpressions;
using System.Xml;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class SberAst : Parser<string, XmlNode>
{
    public SberAst(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
        SearchText = "1С Программное программных программного неисключительное неисключительных битрикс эвм 1С:";
    }

    public string Name => "Sber-AST";

    public string SearchText { get; set; }

    public override async Task<IReadOnlyCollection<Lot>> GetLotsAsync(IReadOnlyCollection<XmlNode> rawLots, CancellationToken ct)
    {
        var ids = new List<Lot>();

        foreach (XmlNode n in rawLots)
        {
            var id = n.SelectSingleNode("_id").InnerText;
            var source = n.SelectSingleNode("_source");
            var purchCodeTerm = source?.SelectSingleNode("purchCodeTerm")?.InnerText ?? "пусто";
            var bidName = source?.SelectSingleNode("BidName")?.InnerText ?? "пусто";
            var objectHrefTerm = source?.SelectSingleNode("objectHrefTerm")?.InnerText ?? "пусто";
            var PublicDate = source?.SelectSingleNode("PublicDate")?.InnerText ?? "пусто";
            var OrgName = source?.SelectSingleNode("OrgName")?.InnerText ?? "пусто";
            var purchAmount = source?.SelectSingleNode("purchAmount")?.InnerText ?? "пусто";

            var lot = new Lot
            {
                Id = id,
                InnerId = id,
                Name = bidName,
                Link = objectHrefTerm,
                PublicationDate = PublicDate,
                CompanyName = OrgName,
                Price = purchAmount
            };

            ids.Add(lot);
        }

        return ids;
    }

    public override async Task<IReadOnlyCollection<XmlNode>> GetProcessedDataAsync(string rawData, CancellationToken ct)
    {
        var json = JObject.Parse(rawData);
        var data = json["data"];
        var rawtext = Regex.Unescape(data.ToString());
        rawtext = rawtext.Replace("\r\n", string.Empty);
        var pattern = "<datarow>.*</datarow>";
        var match = Regex.Match(rawtext, pattern);

        var xDoc = new XmlDocument();
        xDoc.LoadXml(match.Value);
        var xRoot = xDoc.DocumentElement;

        var childnodes = xRoot.SelectNodes("hits");

        var ids = new List<XmlNode>();

        foreach (XmlNode n in childnodes)
            ids.Add(n);

        return ids;
    }

    public override async Task<string> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var par = new Dictionary<string, string>();

        par.Add("xmlData",
            "<elasticrequest> <filters> <mainSearchBar> <value>1С Программное программных программного неисключительное неисключительных битрикс эвм 1С: </value> <type>best_fields</type> <minimum_should_match>1%</minimum_should_match> </mainSearchBar> <purchAmount> <minvalue></minvalue> <maxvalue></maxvalue> </purchAmount> <PublicDate> <minvalue>01.01.2021 00:00</minvalue> <maxvalue></maxvalue> </PublicDate> <PurchaseStageTerm> <value></value> <visiblepart></visiblepart> </PurchaseStageTerm> <SourceTerm> <value></value> <visiblepart></visiblepart> </SourceTerm> <RegionNameTerm> <value></value> <visiblepart></visiblepart> </RegionNameTerm> <RequestStartDate> <minvalue></minvalue> <maxvalue></maxvalue> </RequestStartDate> <RequestDate> <minvalue></minvalue> <maxvalue></maxvalue> </RequestDate> <AuctionBeginDate> <minvalue></minvalue> <maxvalue></maxvalue> </AuctionBeginDate> <okdp2MultiMatch> <value></value> </okdp2MultiMatch> <okdp2tree> <value></value> <productField></productField> <branchField></branchField> </okdp2tree> <classifier> <visiblepart></visiblepart> </classifier> <orgCondition> <value></value> </orgCondition> <orgDictionary> <value></value> </orgDictionary> <organizator> <visiblepart></visiblepart> </organizator> <CustomerCondition> <value></value> </CustomerCondition> <CustomerDictionary> <value></value> </CustomerDictionary> <customer> <visiblepart></visiblepart> </customer> <PurchaseWayTerm> <value></value> <visiblepart></visiblepart> </PurchaseWayTerm> <PurchaseTypeNameTerm> <value></value> <visiblepart></visiblepart> </PurchaseTypeNameTerm> <BranchNameTerm> <value></value> <visiblepart></visiblepart> </BranchNameTerm> <IsSMPTerm> <value></value> <visiblepart></visiblepart> </IsSMPTerm> <statistic> <totalProc>59</totalProc> <TotalSum>72.24 Млн.</TotalSum> <DistinctOrgs>39</DistinctOrgs> </statistic> </filters> <fields> <field>TradeSectionId</field> <field>purchAmount</field> <field>purchCurrency</field> <field>purchCodeTerm</field> <field>PurchaseTypeName</field> <field>purchStateName</field> <field>BidStatusName</field> <field>OrgName</field> <field>SourceTerm</field> <field>PublicDate</field> <field>RequestDate</field> <field>RequestStartDate</field> <field>RequestAcceptDate</field> <field>EndDate</field> <field>CreateRequestHrefTerm</field> <field>CreateRequestAlowed</field> <field>purchName</field> <field>BidName</field> <field>SourceHrefTerm</field> <field>objectHrefTerm</field> <field>needPayment</field> <field>IsSMP</field> <field>isIncrease</field> </fields> <sort> <value>PublicDate</value> <direction></direction> </sort> <aggregations> <empty> <filterType>filn</filterType> <field></field> </empty> </aggregations> <size>20</size> <from>0</from> </elasticrequest>");
        par.Add("orgId", "0");
        par.Add("targetPageCode", "UnitedPurchaseList");

        Client.DefaultRequestHeaders.Add("User-Agent",
            "Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 YaBrowser/19.9.1.237 Yowser/2.5 Safari/537.36");
        
        var response = await Client.PostAsync("/SearchQuery.aspx?name=Main", new FormUrlEncodedContent(par), ct);

        if (response.IsSuccessStatusCode)
        {
            var responseText = await response.Content.ReadAsStringAsync(ct);

            return responseText;
        }

        return string.Empty;
    }
}