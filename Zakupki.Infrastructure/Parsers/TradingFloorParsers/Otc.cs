﻿using System.Globalization;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using Zakupki.Domain;
using Zakupki.Utilites;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class Otc : Parser<string, JToken>
{
    private readonly Dictionary<string, string> cache = new();
    private CultureInfo cultureInfo;

    public Otc(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
        cultureInfo = CultureInfo.CreateSpecificCulture("en-US");
    }

    public override async Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<JToken> rawLots,
        CancellationToken ct)
    {
        return rawLots
            .Where(x => !ContainedInTheCache(x.ToString()))
            .Select(ParsingLotFromString)
            .Where(x => x != null)
            .ToList();
    }

    public override async Task<IReadOnlyCollection<JToken>> GetProcessedDataAsync(string rawData, CancellationToken ct)
    {
        try
        {
            var json = JObject.Parse(rawData);
            var itemsJson = json["Result"]["Tenders"];

            return itemsJson
                .ToArray()
                .Select(x => x)
                .ToList();
        }
        catch (Exception e)
        {
            Logger.Error(e, "Парсинг ответа с площадки {FloorName} не удался", GetType().Name);

            throw;
        }
    }

    public override async Task<string> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var str = "{\"filter_settings\":{\"isPersonal\":true,\"state\":1,\"keywords_list\":" + searchText +
                  ",\"universal_region\":null,\"delivery_city\":null,\"delivery_region\":null,\"okpd2\":null,\"is_doc_search\":false,\"purchase_methods\":null,\"currency_code\":null,\"organization_levels\":[],\"isApplicationWithoutSignatureAllowed\":false,\"publish_date_from\":\"" +
                  DateTime.Now.AddDays(-2).ToString("yyyy-MM-dd") + "\",\"publish_date_to\":\"" +
                  DateTime.Now.AddDays(2).ToString("yyyy-MM-dd") +
                  "\",\"sales_channels\":[],\"region\":null},\"page_settings\":{\"sorting_field\":\"DatePublished\",\"sorting_direction\":\"Desc\",\"page_size\":20,\"page_index\":1,\"not_get_content_highlights\":true},\"saveToHistory\":true,\"reportName\":null}";
        
        var content = new StringContent(str);
        content.Headers.ContentType.MediaType = "application/json";
        
        var resonse = await Client.PostAsync(
            "/microservices-otc/order/api/order/Search",
            content,
            ct);
        
        var result = await resonse.Content.ReadAsStringAsync(ct);

        return result;
    }

    private Lot ParsingLotFromString(JToken value)
    {
        var json = value;

        try
        {
            return ParsingLotFromJson(json);
        }
        catch (Exception e)
        {
            var jsonLot = json.ToString();
            var hash = MD5Util.Calcule(jsonLot);

            if (cache.TryAdd(hash, jsonLot))
            {
                Logger.Error(e,
                    "{TradingFlorName} не удалось распарсить данные",
                    GetType().Name);
            }

            return null;
        }
    }

    private Lot ParsingLotFromJson(JToken json)
    {
        var id = GetValueFromJson(json, "Id");
        var innerId = GetValueFromJson(json, "TenderId");
        var name = GetValueFromJson(json, "LotName");
        var companyName = GetValueFromJson(json, "OrganizerName", false);
        var price = GetValueFromJson(json, "Price");
        var link = "https://otc.ru/tender/" + id;
        var datePublicationString = GetValueFromJson(json, "PublishDate");

        return new Lot
        {
            Id = id,
            InnerId = innerId,
            Name = name,
            CompanyName = companyName,
            Price = price,
            Link = link,
            PublicationDate = datePublicationString,
            TradingFloorInfoId = -1
        };
    }

    private string GetValueFromJson(JToken json, string firstKey, bool withException = true)
    {
        try
        {
            return json[firstKey].ToString();
        }
        catch
        {
            if (withException)
                throw new Exception($"Не удалось распарсить поле {firstKey} из json");

            return "Пустое значение";
        }
    }

    private string GetValueFromJson(JToken json, string firstKey, string secondKey, bool withException = true)
    {
        try
        {
            return (string)json[firstKey][secondKey];
        }
        catch
        {
            if (withException)
                throw new Exception($"Не удалось распарсить поле {firstKey}/{secondKey} из json");

            return "Пустое значение";
        }
    }
}