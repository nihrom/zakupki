﻿using System.Web;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class TekTorg : Parser<IHtmlElement, IElement>
{
    public TekTorg(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
    }

    public override Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<IElement> rawLots,
        CancellationToken ct)
    {
        throw new NotImplementedException();
    }

    public override async Task<IReadOnlyCollection<IElement>> GetProcessedDataAsync(IHtmlElement rawData, CancellationToken ct)
    {
        var trs = rawData.GetElementsByClassName("section-procurement__row");

        var lots = trs
            .Select(tr => tr)
            .ToList();

        return lots;
    }

    public override async Task<IHtmlElement> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var document = await GetDateFromTradingPlaceAsync();

        return document.Body;
    }

    private async Task<IDocument> GetDateFromTradingPlaceAsync()
    {
        var urlencode = HttpUtility.UrlEncode("1С");
        var url = "https://www.tektorg.ru/procedures?lang=ru&q=" + urlencode + "&dpfrom=" +
                  DateTime.Now.AddDays(-2).ToString("dd.MM.yyyy") + "&theme=table";
        Console.WriteLine(url);

        var config = Configuration.Default.WithDefaultLoader();
        var context = BrowsingContext.New(config);
        var document = await context.OpenAsync(url);

        return document;
    }
}