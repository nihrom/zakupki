﻿using System.Net.Http.Json;
using Microsoft.Extensions.Caching.Memory;
using Newtonsoft.Json.Linq;
using Zakupki.Application;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class Berezka : Parser<string, JToken>
{
    public Berezka(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
    }

    public override async Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<JToken> rawLots,
        CancellationToken ct)
    {
        return rawLots
            .Select(ParsingLotFromString)
            .Where(x => x != null)
            .ToList()!;
    }

    public override async Task<IReadOnlyCollection<JToken>> GetProcessedDataAsync(string rawData, CancellationToken ct)
    {
        try
        {
            var json = JObject.Parse(rawData);
            var itemsJson = json["items"];

            return itemsJson == null
                ? ArraySegment<JToken>.Empty
                : itemsJson
                    .ToArray()
                    .Select(x => x)
                    .ToList();
        }
        catch (Exception e)
        {
            Logger.Error(
                e,
                "{ParserName} не удалось распарсить ответ с площадки",
                GetType().Name);
        }
        
        return ArraySegment<JToken>.Empty;
    }

    public override async Task<string> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var str = """{"page":1,"size":10,"isReviewAwaiting":false,"isCustomerSendingAwaiting":false,"isCustomerSigningAwaiting":false,"isSupplierSigningAwaiting":false,"isChangeDealTermsProtocolReceived":false,"isWinner":false,"isLoser":false,"searchText":null,"purchaseName":"","number":null,"lotItemEatCodes":[],"productCode":null,"okpd2Codes":[],"ktruCodes":[],"purchaseTypeIds":[],"types":[],"customerId":null,"customerNameOrInn":null,"customerInn":null,"customerKpp":null,"supplierNameOrInn":null,"purchaseMethods":[],"priceStart":null,"priceEnd":null,"deliveryAddressRegionCodes":[],"deliveryAddress":null,"contractPriceStart":null,"contractPriceEnd":null,"applicationFillingStartDate":null,"applicationFillingEndDate":null,"contractSignDateStart":null,"contractSignDateEnd":null,"deliveryDateStart":null,"deliveryDateEnd":null,"isSmpOnly":false,"isEatOnly":false,"stateDefenseOrderOnly":null,"createDateTime":null,"excludeCancelledByCustomer":false,"excludeExternalTrades":false,"publishDateBegin":null,"publishDateEnd":null,"updateDateBegin":null,"updateDateEnd":null,"applicationFillingStartDateBegin":null,"applicationFillingStartDateEnd":null,"customerContractNumber":null,"hasLinkedExternalTrade":null,"eisTradeNumber":null,"isSpecificSupplier":false,"isRussianItemsPurchase":null,"organizerRegions":[],"organizerRegion":null,"lotStates":[],"sort":[{"fieldName":"publishDate","direction":2}]}""";
            // "{\"page\":1,\"size\":10,\"isReviewAwaiting\":false,\"isCustomerSigningAwaiting\":false,\"isSupplierSigningAwaiting\":false,\"isChangeDealTermsProtocolReceived\":false,\"searchText\":\"" +
            // searchText +
            // "\",\"purchaseName\":\"\",\"lotItemEatCodes\":[],\"okpd2Codes\":[],\"ktruCodes\":[],\"purchaseTypeIds\":[],\"types\":[],\"customerInn\":null,\"customerKpp\":null,\"supplierNameOrInn\":null,\"purchaseMethods\":[1],\"priceStart\":null,\"priceEnd\":null,\"deliveryAddressRegionCodes\":[],\"contractPriceStart\":null,\"contractPriceEnd\":null,\"applicationFillingStartDate\":\"2021-05-05T00:00:00.000Z\",\"applicationFillingEndDate\":null,\"contractSignDateStart\":null,\"contractSignDateEnd\":null,\"deliveryDateStart\":null,\"deliveryDateEnd\":null,\"isSmpOnly\":false,\"stateDefenseOrderOnly\":false,\"createDateTime\":null,\"excludeCancelledByCustomer\":false,\"lotStates\":[2],\"sort\":[{\"fieldName\":\"publishDate\",\"direction\":2}]}";

        var content = new StringContent(str);
        content.Headers.ContentType!.MediaType = "application/json";
        var response = await Client.PostAsync("/api/TradeLot/list-published-trade-lots", content, ct);
        //var result = await response.Content.ReadAsStringAsync(ct);
        response.EnsureSuccessStatusCode();

        var result = await response.Content.ReadAsStringAsync(ct);
        
        return result;
    }

    private Lot? ParsingLotFromString(JToken value)
    {
        try
        {
            return ParsingLotFromJson(value);
        }
        catch (Exception e)
        {
            var jsonLot = value.ToString();

            Logger.Error(e,
                "{ParserName}: не удалось раcпарсить данные. Исходный объект - {JsonLot}",
                GetType().Name, jsonLot);
        }

        return null;
    }

    private Lot ParsingLotFromJson(JToken json)
    {
        var id = GetValueFromJson(json, "id");
        var innerId = GetValueFromJson(json, "tradeNumber");
        var name = GetValueFromJson(json, "subject");
        var companyName = GetValueFromJson(json, "organizerInfo", "fullName", false);
        var price = GetValueFromJson(json, "price");
        var link = "https://agregatoreat.ru/purchases/announcement/" + GetValueFromJson(json, "id") + "/info";
        var datePublication = GetValueFromJson(json, "publishDate");

        return new Lot
        {
            Id = id,
            InnerId = innerId,
            Name = name,
            CompanyName = companyName,
            Price = price,
            Link = link,
            PublicationDate = datePublication,
            TradingFloorInfoId = -1
        };
    }

    private string GetValueFromJson(JToken json, string firstKey, bool withException = true)
    {
        try
        {
            return json[firstKey].ToString();
        }
        catch
        {
            if (withException)
                throw new Exception($"Не удалось распарсить поле {firstKey} из json");

            return "Пустое значение";
        }
    }

    private string GetValueFromJson(JToken json, string firstKey, string secondKey, bool withException = true)
    {
        try
        {
            return (string)json[firstKey][secondKey];
        }
        catch
        {
            if (withException)
                throw new Exception($"Не удалось распарсить поле {firstKey}/{secondKey} из json");

            return "Пустое значение";
        }
    }
}