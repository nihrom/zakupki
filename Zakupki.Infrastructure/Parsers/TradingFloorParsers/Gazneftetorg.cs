﻿using System.Web;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class Gazneftetorg : Parser<IHtmlElement, IElement>
{
    public Gazneftetorg(HttpClient client, IMemoryCache? cache) : base(client, cache)
    {
    }

    public override Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<IElement> rawLots,
        CancellationToken ct)
    {
        throw new NotImplementedException();
    }

    public override async Task<IReadOnlyCollection<IElement>> GetProcessedDataAsync(IHtmlElement rawData, CancellationToken ct)
    {
        var trs = rawData.GetElementsByClassName("c1");

        var lots = trs
            .Select(tr => tr)
            .ToList();

        return lots;
    }

    public override async Task<IHtmlElement> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var document = await GetDateFromTradingPlaceAsync(searchText);

        return document.Body!;
    }

    private async Task<IDocument> GetDateFromTradingPlaceAsync(string searchText)
    {
        var urlEncode = HttpUtility.UrlEncode(searchText);
        var url = "https://www.gazneftetorg.ru/search/trades?keywords=" + urlEncode +
                  "&search_input_submit.x=24&search_input_submit.y=21&date_type=date_publication_ts&date_from=" +
                  DateTime.Now.AddDays(-2).ToString("dd.MM.yyyy") + "&date_to=&search_in_archive=0";

        var config = Configuration.Default.WithDefaultLoader();
        var context = BrowsingContext.New(config);
        var document = await context.OpenAsync(url);

        return document;
    }
}