﻿using System.Web;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class EtpGpb : Parser<string, IElement>
{
    public EtpGpb(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
    }
    
    public override async Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<IElement> rawLots,
        CancellationToken ct)
    {
        var trs = rawLots;
    
        return trs.Select(p =>
            {
                var company = p.QuerySelector(".procedure__companyName").TextContent;
                var company2 = p.QuerySelector(".procedure__infoDescriptionFull").TextContent;
                var company3 = p.QuerySelector(".procedure__infoDescriptionCode").TextContent;
                var company4 = p.QuerySelector(".procedure__infoTitle").TextContent;
                var company5 = p.QuerySelector(".procedure__detailsSum")?.TextContent ?? "0";
                var company6 = p.QuerySelector(".procedure__link").GetAttribute("href");
    
                string link;
                if (company6.Contains("http"))
                    link = company6;
                else
                    link = "https://etpgpb.ru" + company6;
    
                return new Lot
                {
                    Id = company3,
                    InnerId = company4,
                    CompanyName = company,
                    Name = company2,
                    Price = company5,
                    Link = link,
                    TradingFloorInfoId = -1
                };
            })
            .ToList();
    }
    
    public override async Task<IReadOnlyCollection<IElement>> GetProcessedDataAsync(string rawData, CancellationToken ct)
    {
        var document = await new HtmlParser().ParseDocumentAsync(rawData, ct);
        var trs = document.Body.GetElementsByClassName("procedure");
    
        var lots = trs
            .Select(tr => tr)
            .ToList();
    
        return lots;
    }
    
    public override async Task<string> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var urlEncode = HttpUtility.UrlEncode(searchText);
        var response =
            await Client.GetAsync("/procedures/?procedure%5Dcategory%5D=actual&procedure%5Dpublished_from%5D=" +
                                  DateTime.Now.AddDays(-12).ToString("dd.MM.yyyy") + "&search=" + urlEncode);
    
        if (response.IsSuccessStatusCode)
        {
            var responseText = await response.Content.ReadAsStringAsync(ct);
    
            return responseText;
        }
    
        return string.Empty;
    }
}