﻿using System.Web;
using AngleSharp.Dom;
using AngleSharp.Html.Parser;
using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class EtpEts : Parser<string, IElement>
{
    public EtpEts(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
    }
    
    public override async Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<IElement> rawLots,
        CancellationToken ct)
    {
        var lots = rawLots
            .Skip(1)
            .Where(x => !ContainedInTheCache(x.TextContent))
            .Select(tr => new Lot
            {
                Link = tr.QuerySelector(".row-name").QuerySelector("a").GetAttribute("href"),
                Id = tr.QuerySelector(".row-name").LastChild.TextContent.Replace(" ", string.Empty),
                Name = tr.QuerySelector(".row-name").QuerySelector("a").TextContent,
                Price = tr.QuerySelector(".row-maxSum").TextContent,
                CompanyName = tr.QuerySelector(".row-customer").QuerySelector("a").TextContent,
                PublicationDate = tr.QuerySelector(".row-publicationDateTime").TextContent,
                TradingFloorInfoId = -1,
            })
            .ToList();
    
        return lots;
    }
    
    public override async Task<IReadOnlyCollection<IElement>> GetProcessedDataAsync(
        string rawData,
        CancellationToken ct)
    {
        var document = await new HtmlParser().ParseDocumentAsync(rawData, ct);
        var trs = document.Body.QuerySelectorAll("tr");
    
        var lots = trs
            .Skip(1)
            .Select(tr => tr)
            .ToList();
    
        return lots;
    }
    
    public override async Task<string> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var urlEncode = HttpUtility.UrlEncode(searchText);
        var url = "/procedure/catalog/?&keywords=" + urlEncode + "&publicationDateTime-from=" +
                  DateTime.Now.AddDays(-10).ToString("dd.MM.yyyy") + "&fullSearch=1";
        var response = await Client.GetAsync(url, ct);
    
        if (!response.IsSuccessStatusCode)
            return string.Empty;
    
        return await response.Content.ReadAsStringAsync(ct);
    }
}