﻿using System.Web;
using AngleSharp;
using AngleSharp.Dom;
using AngleSharp.Html.Dom;
using Microsoft.Extensions.Caching.Memory;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.Parsers.TradingFloorParsers;

public class Roseltorg : Parser<IHtmlElement, IElement>
{
    public Roseltorg(HttpClient client, IMemoryCache? memoryCache) : base(client, memoryCache)
    {
    }

    public override Task<IReadOnlyCollection<Lot>> GetLotsAsync(
        IReadOnlyCollection<IElement> rawLots,
        CancellationToken ct)
    {
        throw new NotImplementedException();
    }

    public override async Task<IReadOnlyCollection<IElement>> GetProcessedDataAsync(IHtmlElement rawData, CancellationToken ct)
    {
        var trs = rawData.GetElementsByClassName("search-results__item");

        var lots = trs
            .Select(tr => tr)
            .ToList();

        return lots;
    }

    public override async Task<IHtmlElement> GetRawDataAsync(string searchText, CancellationToken ct)
    {
        var document = await GetDateFromTradingPlaceAsync();

        return document.Body;
    }

    private async Task<IDocument> GetDateFromTradingPlaceAsync()
    {
        var urlencode = HttpUtility.UrlEncode("1С");
        var url = "https://roseltorg.ru/procedures/search?query_field=" + urlencode +
                  "&customer=&status%5B%5D=0&status%5B%5D=1&start_price=&end_price=&currency=all&start_date_published=" +
                  DateTime.Now.AddDays(-2).ToString("dd.MM.yyyy") +
                  "&start_date_requests=&guarantee_start_price=&guarantee_end_pricee=&form_id=searchp_form&from=0&query_field=" +
                  urlencode +
                  "&customer=&status%5B%5D=0&status%5B%5D=1&address=&start_price=&end_price=&currency=all&start_date_published=" +
                  DateTime.Now.AddDays(-25).ToString("dd.MM.yyyy") +
                  "&end_date_published=&guarantee_start_price=&guarantee_end_price=&start_date_requests=&end_date_requests=&form_id=searchp_form&page=";
        Console.WriteLine(url);

        var config = Configuration.Default.WithDefaultLoader();
        var context = BrowsingContext.New(config);
        var document = await context.OpenAsync(url);

        return document;
    }
}