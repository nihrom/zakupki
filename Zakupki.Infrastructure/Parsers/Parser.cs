﻿using System.Diagnostics;
using Microsoft.Extensions.Caching.Memory;
using Serilog;
using Zakupki.Application.Services;
using Zakupki.Domain;
using Zakupki.Utilites;

namespace Zakupki.Infrastructure.Parsers
{
    public abstract class Parser<TRaw, TProcessed> : IParser
    {
        protected readonly ILogger Logger;
        protected readonly HttpClient Client;
        
        private readonly IMemoryCache? cache;
        private Stopwatch stopwatch = new();

        private readonly MD5Util mD5Util = new();

        protected Parser(HttpClient client, IMemoryCache? cache)
        {
            this.cache = cache;
            Client = client;
            Logger = Log.ForContext<Parser<TRaw, TProcessed>>();
            Logger.Information(
                "{ParserName} парсер -------------------------- создан",
                GetType().Name);
        }

        protected bool ContainedInTheCache(string value)
        {
            if (cache == null)
                return false;

            var hash = mD5Util.CalculeLocal(value);

            if (cache.TryGetValue(hash, out string val))
            {
                Logger.Verbose(
                    "{ParserName} - {Hash} содержится в кэше",
                    GetType().Name,
                    hash);
                
                return true;
            }
            else
            {
                Logger.Verbose(
                    "{ParserName} - {Hash} не содержится в кэше",
                    GetType().Name,
                    hash);

                var cacheEntryOptions = new MemoryCacheEntryOptions()
                    .SetSlidingExpiration(TimeSpan.FromMinutes(10));

                cache.Set(hash, string.Empty, cacheEntryOptions);
                return false;
            }
        }

        public async Task<IReadOnlyCollection<Lot>> ParseLotsAsync(string searchText, CancellationToken ct)
        {
            stopwatch = Stopwatch.StartNew();

            Logger.Debug(
                "{ParserName} парсит - {SearchText}",
                GetType().Name,
                searchText);
            try
            {
                var rawData = await GetRawDataAsync(searchText, ct);

                Logger.Debug(
                    "{ParserName} спарсил - {SearchText}. Time: {Time}ms",
                    GetType().Name,
                    searchText,
                    stopwatch.Elapsed.TotalMilliseconds);

                var rawLots = await GetProcessedDataAsync(rawData, ct);

                Logger.Debug(
                    "{ParserName} распарсил ответ. Количество лотов: {Count}. Time: {Time}ms",
                    GetType().Name,
                    rawLots.Count,
                    stopwatch.Elapsed.TotalMilliseconds);

                var filteredLots = rawLots
                    //.Where(l => !ContainedInTheCache(l)) //TODO: возможно таки как нибудь вернуть кэширование
                    .ToList();

                var lots = await GetLotsAsync(filteredLots, ct);

                Logger.Debug(
                    "{ParserName} вернул {Count} лотов. Time: {Time}ms",
                    GetType().Name,
                    filteredLots.Count,
                    stopwatch.Elapsed.TotalMilliseconds);

                return lots;
            }
            catch (Exception exception)
            {
                Logger.Error(
                    exception,
                    "{ParserName} не смог спарсить площадку",
                    GetType().Name);
            }

            stopwatch.Stop();

            return Array.Empty<Lot>();
        }

        public async Task<string> ParseRawDataAsync(string searchText, CancellationToken ct)
        {
            stopwatch = Stopwatch.StartNew();

            Logger.Debug(
                "{ParserName} парсит - {SearchText}",
                GetType().Name,
                searchText);
            try
            {
                var rawData = await GetRawDataAsync(searchText, ct);

                Logger.Debug(
                    "{ParserName} спарсил - {SearchText}. Time: {Time}ms",
                    GetType().Name,
                    searchText,
                    stopwatch.Elapsed.TotalMilliseconds);

                return rawData.ToString();
            }
            catch (Exception exception)
            {
                Logger.Error(
                    exception,
                    "{ParserName} не смог спарсить площадку",
                    GetType().Name);
            }

            stopwatch.Stop();

            return "Ошибка";
        }

        public async Task<IReadOnlyCollection<string>> ParseProcessedDataAsync(string searchText, CancellationToken ct)
        {
            stopwatch = Stopwatch.StartNew();

            Logger.Debug(
                "{ParserName} парсит - {SearchText}",
                GetType().Name,
                searchText);
            try
            {
                var rawData = await GetRawDataAsync(searchText, ct);

                Logger.Debug(
                    "{ParserName} спарсил - {SearchText}. Time: {Time}ms",
                    GetType().Name,
                    searchText,
                    stopwatch.Elapsed.TotalMilliseconds);

                var rawLots = await GetProcessedDataAsync(rawData, ct);

                Logger.Debug(
                    "{ParserName} распарсил ответ. Количество лотов: {Count}. Time: {Time}ms",
                    GetType().Name,
                    rawLots.Count,
                    stopwatch.Elapsed.TotalMilliseconds);

                return rawLots.Select(l => l.ToString()).ToList();
            }
            catch (Exception exception)
            {
                Logger.Error(
                    exception,
                    "{ParserName} не смог спарсить площадку",
                    GetType().Name);
            }

            stopwatch.Stop();

            return Array.Empty<string>();
        }

        public abstract Task<IReadOnlyCollection<Lot>> GetLotsAsync(
            IReadOnlyCollection<TProcessed> rawLots,
            CancellationToken ct);

        public abstract Task<IReadOnlyCollection<TProcessed>> GetProcessedDataAsync(
            TRaw rawData,
            CancellationToken ct);

        public abstract Task<TRaw> GetRawDataAsync(string searchText, CancellationToken ct);
    }
}