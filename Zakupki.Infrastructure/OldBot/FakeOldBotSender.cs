﻿using Serilog;
using Zakupki.Application.Services;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.OldBot;

public class FakeOldBotSender : IOldBotSender
{
    private readonly ILogger logger = Log.ForContext<FakeOldBotSender>();

    // public FakeOldBotSender(ILogger logger)
    // {
    //     this.logger = logger;
    // }

    public Task<bool> SendLotAsync(Lot lot)
    {
        logger.Information("Фейковый сервис отправки на внешний бот типа отправляет лот");

        return Task.FromResult(true);
    }
}