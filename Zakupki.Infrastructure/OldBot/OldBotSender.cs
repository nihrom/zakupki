﻿using Serilog;
using Zakupki.Application.Services;
using Zakupki.Domain;

namespace Zakupki.Infrastructure.OldBot;

public class OldBotSender : IOldBotSender
{
    private readonly HttpClient client;
    private readonly ILogger logger = Log.ForContext<OldBotSender>();

    public OldBotSender(IHttpClientFactory httpClientFactory)
    {
        _ = httpClientFactory ?? throw new ArgumentNullException(nameof(httpClientFactory));
        client = httpClientFactory.CreateClient("OldBotSender");
    }

    public async Task<bool> SendLotAsync(Lot lot)
    {
        try
        {
            var result = await client.GetAsync(
                $"berezka-out.php?" +
                $"name={lot.Name}&companyName={lot.CompanyName}&price={lot.Price}&id={lot.Id}&link={lot.Link}");

            logger.Information(
                "Сервис отправки на внешний бот закончился с кодом {StatusCode} и результатом {Result}", 
                result.StatusCode,
                await result.Content.ReadAsStringAsync());

            return result.IsSuccessStatusCode;
        }
        catch (Exception ex)
        {
            logger.Information(
                "Сервис отправки на внешний бот  не смог отправить по неизвестной причине - ошибка: {Message}", ex.Message);

            return false;
        }
    }
}