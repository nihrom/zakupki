﻿using MailKit.Net.Smtp;
using MimeKit;
using MimeKit.Text;
using Zakupki.Application.Services;

namespace Zakupki.Infrastructure.MailService;

public class EmailService : IEmailService
{
    private string emailTo;

    //public EmailService(string emailTo)
    //{
    //    this.emailTo = emailTo;
    //}

    public async Task SendEmailAsync(string company, string link)
    {
        var emailMessage = new MimeMessage();

        emailMessage.From.Add(new MailboxAddress("Татьяна Полухина", "tanuki2608@mail.ru"));
        emailMessage.To.Add(new MailboxAddress("Татьяна Полухина", "tanuki2608@mail.ru"));
        emailMessage.Subject = company;
        emailMessage.Body = new TextPart(TextFormat.Html)
        {
            Text = $"<a href='{link}'>{link}</a><br><br>--<br>С Уважением,<br>Татьяна"
        };

        using (var client = new SmtpClient())
        {
            await client.ConnectAsync("smtp.mail.ru", 465, true);
            await client.AuthenticateAsync(
                "tanuki2608@mail.ru",
                "6105502tanya"); //"5N9uJ42GrZqC");//"kV3J$qwEFMEjwr#z");

            await client.SendAsync(emailMessage);

            await client.DisconnectAsync(true);
        }
    }

    public async Task SendRequestForRepair()
    {
        //await SendEmailAsync(emailTo, "Заявка с Гидросанцентра", "Телефон - " + request.Phone + " Имя - " + request.Name);
    }
}